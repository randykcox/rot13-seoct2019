/*

X 1. When a button is pressed, read text from a text box
    X a. Click handler on the button
        - addEventListener ('click', ___)
    X b. retrieve value of the text box
        - <textarea>
        - textAreaElement.value
        - put the text into a variable
    X c. Pass the text on to the next step

2. Run the rot13 algorithm on the text to scramble it
    (We have the text in a string)
    X a. Loop through the letters of the plaintext string 
    b. Send each letter to a function that takes a single letter and returns the "scrambled" letter
    X c. Save the letters to a "scrambled" string

3. Write the scrambled text to the page
    (We have the scrambled text in a string)
    a. Get a reference to a place on the page to output to
        - <div id="output"></div>
        - getElementById('output')
    b. create text node of the scrambled text
        - document.createTextNode(___)
    c. append child to put the scrambled text into the page element

*/

let scrambleLetter = function (letter) {
    let scrambledLetter

    // scramble the letter
    scrambledLetter = letter.toUpperCase()

    return scrambledLetter
}

let processTheText = function () {
    let textareaElement = document.getElementById('plaintext')
    let plaintext = textareaElement.value
    let rot13output = ''

    for (let i=0; i<plaintext.length; i++) {
        let rot13letter = scrambleLetter(plaintext.charAt(i))
        rot13output = rot13output + rot13letter
    }

    console.log(rot13output)

}

let gobutton = document.getElementById('doScramble')
gobutton.addEventListener('click', processTheText)